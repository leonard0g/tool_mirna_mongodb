﻿﻿﻿# miRNA mongodb tool
## Introduction
TBD

## Requirements
This application requires the following modules:

* python 2.7
* g++-4.8 or higher
* setuptools
* mongodb: [installation guide](https://docs.mongodb.com/manual/administration/install-community/)
* pymongo: ```sudo pip install pymongo```
* NumPy 1.7.1: ```sudo pip install numpy```
* pandas 0.18.1: ```sudo pip install pandas```
* scikit-learn 0.18: ```sudo pip install sklearn```
* retrying: ```sudo pip install retrying```
* python-dateutil 1.5
* pytz 2016.6.1

## Optional modules
* [Robomongo](https://robomongo.org/): a graphical interface for mongodb, useful to navigate data and test queries easily.

## Configuration
The only parameters needed are the following ones in ```.config.rc```:
```bash
[db connection params]
host = localhost
port = 27017
db = bioinf
```
* host: change this parameter according to the machine where MongoDB server is running (e.g. 192.168.0.4);
* port: 27017 is the default one for MongoDB server; there is no need to edit this field unless you chose a different port during MongoDB server setup;
* db: database name to use for uploading/querying data.

## Usage
```bash
git clone https://leonard0g@bitbucket.org/leonard0g/tool_mirna_mongodb.git
cd tool_mirna_mongodb
python ui_controller.py
```