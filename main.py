# -*- coding: utf-8 -*-
from my_libs.norm import *
from my_libs.utils import *
import numpy as np
import pandas as pd
import scipy
import math
from sklearn.svm import SVC
from sklearn import cross_validation, feature_selection, preprocessing
from sklearn.feature_selection import VarianceThreshold
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2, f_classif
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.ensemble import ExtraTreesClassifier, RandomForestClassifier, VotingClassifier
from sklearn.feature_selection import SelectFromModel
from sklearn.pipeline import Pipeline
import matplotlib
# matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap
from sklearn import datasets
from sklearn.grid_search import GridSearchCV, RandomizedSearchCV
from sklearn.metrics import classification_report, accuracy_score
from sklearn.metrics import confusion_matrix
from sklearn.cross_validation import train_test_split, cross_val_predict, StratifiedKFold
import operator  # itemgetter
from sklearn.metrics import roc_curve, auc, average_precision_score, roc_auc_score
from scipy import interp
from collections import Counter
from sklearn.preprocessing import StandardScaler, RobustScaler, MinMaxScaler, LabelEncoder
from sklearn.linear_model import Perceptron
from sklearn.linear_model import LogisticRegression
from sklearn.base import clone
from itertools import combinations
import re

__author__ = 'lg'


class Main(object):
	def __init__(self):
		self.status = False
		self.n_folds = 10
		self.reports = {}
		return


	####################################################################################################################
	############ get data ##############################################################################################
	def get_data(self, csv, label, use_isoforms, isoform):
		self.status = False
		try:
			if csv is not None:
				self.data, self.target = load_dataset_from_file(csv)
			else:
				self.data, self.target = load_dataset_from_label(label, isoforms=use_isoforms)
		except Exception as e:
			print e
			self.err_msg = e.message
			self.status = False
			return

		print "done"

		self.feature_names = self.data.columns.values
		self.X = np.array(self.data)
		self.target = np.ravel(
			np.array(self.target))  # ravel traspone da [[1],[2],[3]] a [1, 2, 3] (svm vuole y come vettore riga)
		self.target_names = np.array(np.unique(self.target))

		######################################################
		############ encode labels ###########################
		self.le = LabelEncoder()
		self.le.fit(self.target)
		self.y = np.array(self.le.transform(self.target))
		# print self.le.inverse_transform(self.y)	# ritrasforma da interi a stringhe

		if isoform is not None and len(isoform) > 0:
			############ drop all but the specified isoform ######
			expr = re.compile(".*" + isoform + ".*")
			goodvalues_idx = [i for i, v in enumerate(self.feature_names) if expr.match(v)]
			self.feature_names = np.take(self.feature_names, goodvalues_idx)
			self.X = np.take(self.X, goodvalues_idx, axis=1)  # take only goodvalues_idx *columns*

		self.__stats()

		self.status = True

	####################################################################################################################
	############ preprocessing #########################################################################################
	def preprocessing(self, test_size, scaler, feature_selection_params):
		self.status = False
		print "main:", test_size, scaler, feature_selection_params

		######################################################
		############ split data ##############################
		self.X_train, self.X_test, self.y_train, self.y_test = train_test_split(self.X, self.y, test_size=test_size,
		                                                                        random_state=0)

		######################################################
		############ scale data ##############################
		if scaler is 'standard':
			scaler = StandardScaler()
		elif scaler is 'robust':
			scaler = RobustScaler()  # robust against outliers
		elif scaler is 'minmax':
			scaler = MinMaxScaler(feature_range=(0, 1))

		scaler.fit(self.X_train)
		self.X_train_sc = scaler.transform(self.X_train)
		self.X_test_sc = scaler.transform(self.X_test)

		################################################################################################################
		############ feature selection #################################################################################
		self.fs_results = {}
		try:
			if len(feature_selection_params) == 0:
				self.X_train_sel = self.X_train_sc
				self.X_test_sel = self.X_test_sc
				self.feature_names_sel = self.feature_names
				self.feature_idx = [i for i, v in enumerate(self.feature_names_sel)]
			else:
				if 'variance_threshold' in feature_selection_params:
					self.__fs_var_threshold(feature_selection_params['variance_threshold'])
				if 'kbest' in feature_selection_params:
					self.__fs_kbest(feature_selection_params['kbest'])
				# if 'pca' in feature_selection_params:
				#	pass
		except Exception as e:
			self.status = False
			self.err_msg = e.message
			raise Exception(e.message)

		self.status = True

	####################################################################################################################
	############ support vector machine ################################################################################
	# C: penalty parameter C of the error term (default: 1)
	# degree: degree of the polynomial kernel function (‘poly’). Ignored by all other kernels (default: 3)
	# gamma: kernel coefficient for ‘rbf’, ‘poly’ and ‘sigmoid’. If gamma is ‘auto’ then 1/n_features will be used instead
	def train_svm(self, kernel, C, gamma, degree, n_svm):
		self.status = False
		try:
			if n_svm == 1:
				clf = SVC(kernel=kernel, C=C, degree=degree, gamma=gamma, class_weight='balanced', random_state=0, probability=True)
				clf.fit(self.X_train_sel, self.y_train)

			else:  # majority voter
				clfs = []
				for i in range(n_svm):
					clfs.append((str(i), SVC(kernel=kernel, C=C, degree=degree, gamma=gamma, class_weight='balanced',
					                         random_state=i, probability=True)))
				# hard_voting_clf = VotingClassifier(estimators=clfs, voting='hard')
				clf = VotingClassifier(estimators=clfs, voting='soft')
				clf.fit(self.X_train_sel, self.y_train)

			y_pred = cross_val_predict(clf, self.X_test_sel, self.y_test, cv=10)
			y_pred_proba = clf.predict_proba(self.X_test_sel)[:, 1]

			self.__classification_report(self.y_test, y_pred, y_pred_proba, target_names=list(self.target_names), algorithm='svm')

			self.status = True

		except Exception as e:
			self.status = False
			self.err_msg = e.message
			raise Exception(e.message)


	####################################################################################################################
	############ random forest #########################################################################################
	def train_rf(self, forest_type, criterion, estimators, max_depth, bootstrap):
		self.status = False
		try:
			if forest_type is 'randomized':
				clf = RandomForestClassifier(criterion=criterion, n_estimators=estimators, random_state=1, bootstrap=bootstrap, max_depth=max_depth)
			else:  # forest_type is 'extremely randomized':
				clf = ExtraTreesClassifier(criterion=criterion, n_estimators=estimators, random_state=1, bootstrap=bootstrap, max_depth=max_depth)

			clf.fit(self.X_train_sel, self.y_train)

			y_pred = cross_val_predict(clf, self.X_test_sel, self.y_test, cv=10)
			y_pred_proba = clf.predict_proba(self.X_test_sel)[:, 1]

			self.__classification_report(self.y_test, y_pred, y_pred_proba, target_names=list(self.target_names), algorithm='rf')

			self.status = True

		except Exception as e:
			self.status = False
			self.err_msg = e.message
			raise Exception(e.message)


	####################################################################################################################
	############ neural network ########################################################################################
	def train_nn(self, solver, learning_rate, alpha, hidden_layer_sizes, max_iterations):
		self.status = False
		try:
			clf = MLPClassifier(solver=solver, learning_rate=learning_rate, alpha=alpha, hidden_layer_sizes=hidden_layer_sizes, max_iter=max_iterations)

			clf.fit(self.X_train_sel, self.y_train)

			y_pred = cross_val_predict(clf, self.X_test_sel, self.y_test, cv=10)
			y_pred_proba = clf.predict_proba(self.X_test_sel)[:, 1]

			self.__classification_report(self.y_test, y_pred, y_pred_proba, target_names=list(self.target_names), algorithm='nn')

			self.status = True

		except Exception as e:
			self.status = False
			self.err_msg = e.message
			raise Exception(e.message)


	####################################################################################################################
	############ report ################################################################################################
	def __classification_report(self, y, y_pred, y_pred_proba, target_names, algorithm):
		self.reports[algorithm] = {}
		crosstab_perc = pd.crosstab(y, y_pred, rownames=['True'], colnames=['Predicted']).apply(
			lambda r: 100.0 * r / r.sum())
		crosstab = pd.crosstab(y, y_pred, rownames=['True'], colnames=['Predicted']).apply(lambda r: r)
		sklearn_report = classification_report(y, y_pred, target_names=target_names)

		TP = 0
		FP = 0
		TN = 0
		FN = 0

		for i in range(len(y_pred)):
			if y[i] == y_pred[i] == 1:
				TP += 1
			if y[i] == 1 and y[i] != y_pred[i]:
				FP += 1
			if y[i] == y_pred[i] == 0:
				TN += 1
			if y[i] == 0 and y[i] != y_pred[i]:
				FN += 1

		TPR = round(1.0 * TP / (TP + FN + 0.00000001), 2)  # true positive rate, sensitivity, recall
		TNR = round(1.0 * TN / (TN + FP + 0.00000001), 2)  # true negative rate, specificity
		FPR = round(1.0 * FP / (FP + TN + 0.00000001), 2)  # false positive rate, fallout
		FNR = round(1.0 * FN / (FN + TP + 0.00000001), 2)  # false negative rate
		PPV = round(1.0 * TP / (TP + FP + 0.00000001), 2)  # precision, positive predicted value
		NPV = round(1.0 * TN / (TN + FN + 0.00000001), 2)  # precision, negative predicted value
		accuracy = round(1.0 * (TP + TN) / (TP + FP + TN + FN), 2)
		f1 = round(1.0 * 2 * TP / (2 * TP + FP + FN), 2)

		#roc_fpr, roc_tpr, thresholds = roc_curve(y, y_pred)#, pos_label=2)
		roc_fpr, roc_tpr, _ = roc_curve(y, y_pred_proba)
		roc_area = auc(roc_fpr, roc_tpr)

		self.reports[algorithm] = dict(tp=TP, tn=TN, fp=FP, fn=FN, tpr=TPR, tnr=TNR, fpr=FPR, fnr=FNR, ppv=PPV, npv=NPV,
		                               accuracy=accuracy, f1=f1, crosstab_perc=crosstab_perc, crosstab=crosstab,
		                               sklearn_report=sklearn_report, roc=[roc_fpr, roc_tpr, roc_area])


	####################################################################################################################
	############ variance threshold feature selection ##################################################################
	def __fs_var_threshold(self, threshold):
		feature_idx = []
		# VarianceThreshold feature selection
		# p: remove all features that are the same value in more than p% of the samples
		try:
			print "VarianceThreshold feature selection"
			vt_sel = VarianceThreshold(threshold)
			X_train_sel = vt_sel.fit_transform(self.X_train_sc, self.y_train)
			feature_idx = vt_sel.get_support(indices=True)

			# drop from X_testy except feature_idx
			X_test_sel = np.take(self.X_test_sc, feature_idx, axis=1)  # take only feature_idx *columns*
			print "after VarianceThreshold: ", X_train_sel.shape, X_test_sel.shape

			self.X_train_sel = X_train_sel
			self.X_test_sel = X_test_sel
			self.fs_results['variance_threshold'] = len(feature_idx)
			self.feature_names_sel = np.take(self.feature_names, feature_idx)  # take only feature_idx
			self.feature_idx = feature_idx

		except Exception as e:
			raise e


	####################################################################################################################
	############ kbest feature selection ###############################################################################
	def __fs_kbest(self, k):
		feature_idx = []
		if self.X_train_sc.shape[1] < k:
			raise Exception("In __fs_kbest: number of features < k")
		try:
			print "KBest feature selection"
			# kb_sel = SelectKBest(chi2, k=k)
			kb_sel = SelectKBest(f_classif, k=k)
			X_train_sel = kb_sel.fit_transform(self.X_train_sc, self.y_train)
			feature_idx = kb_sel.get_support(indices=True)

			# drop from X_test except feature_idx
			X_test_sel = np.take(self.X_test_sc, feature_idx, axis=1)
			print "after SelectKBest: ", X_train_sel.shape, X_test_sel.shape

			self.X_train_sel = X_train_sel
			self.X_test_sel = X_test_sel
			self.fs_results['kbest'] = len(feature_idx)
			self.feature_names_sel = np.take(self.feature_names, feature_idx)  # take only feature_idx
			self.feature_idx = feature_idx

		except Exception as e:
			raise e


	####################################################################################################################
	############ make dataset stats ####################################################################################
	def __stats(self):
		self.n_samples = self.X.shape[0]
		self.n_features = self.X.shape[1]
		self.n_isomir = 0
		for f in self.feature_names:
			if "iso" in f or "exact" in f:
				self.n_isomir += 1
		self.label = self.target
		self.n_mirna = self.n_features - self.n_isomir

		self.classes_distr = {}
		for cl in self.target_names:
			self.classes_distr[cl] = len(self.target[self.target == cl])
