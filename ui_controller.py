# -*- coding: utf-8 -*-

import sys
from ConfigParser import SafeConfigParser, ParsingError
from PyQt4 import QtGui, QtCore

from PyQt4.QtCore import QThread, pyqtSlot
from PyQt4.QtGui import *

import constants
from my_libs.plots import *
from ui.ui import Ui_mainWindow
from main import Main

__author__ = 'lg'


class GenericThread(QThread):
	def __init__(self, function, *args, **kwargs):
		QThread.__init__(self)
		QThread.setTerminationEnabled(True)
		self.function = function
		self.args = args
		self.kwargs = kwargs

	# self.stop = QtCore.pyqtSignal(int)

	def __del__(self):
		self.wait()

	def run(self):
		self.function(*self.args, **self.kwargs)
		# self.stop.emit(False)
		self.emit(QtCore.SIGNAL('terminated'))
		return


class Progress(QtCore.QObject):
	# create a new signal on the fly and name it
	showProgress = QtCore.pyqtSignal(str)
	closeProgress = QtCore.pyqtSignal()


class UiController(QtGui.QMainWindow):
	def __init__(self, parent=None):
		QtGui.QMainWindow.__init__(self, parent)
		self.ui = Ui_mainWindow()
		self.ui.setupUi(self)
		self.__center_on_screen()

		self.__parser = SafeConfigParser()
		self.__parser.read(constants.MIML_LOG_FILE)

		self.__main = Main()

		self.__setup()
		self.__add_handlers()
		self.genericThread = None

	def __center_on_screen(self):
		resolution = QtGui.QDesktopWidget().screenGeometry()
		self.move((resolution.width() / 2) - (self.frameSize().width() / 2),
		          (resolution.height() / 2) - (self.frameSize().height() / 2))


	def __setup(self):
		self.ui.tabWidget.setCurrentIndex(0)
		self.ui.gb_stats.setVisible(False)
		self.ui.gb_features.setEnabled(False)
		self.ui.gb_from_db.setEnabled(False)
		self.ui.gb_from_csv.setEnabled(False)
		self.ui.tab_preprocessing.setEnabled(False)
		self.ui.tab_svm.setEnabled(False)
		self.ui.tab_rf.setEnabled(False)
		self.ui.tab_nn.setEnabled(False)
		self.ui.combo_isoform.clear()
		self.ui.combo_isoform.addItems(['', 'mirna_exact', 'iso_5p', 'iso_3p', 'iso_snp', 'iso_multi_snp'])


	def __add_handlers(self):
		# data tab
		self.ui.radio_from_csv.toggled.connect(self.__radio_from_csv_clicked)
		self.ui.b_choose_csv.clicked.connect(self.__pick_csv)
		self.ui.from_database_radio.toggled.connect(self.__radio_from_database_clicked)
		self.ui.radio_mirna.toggled.connect(self.__radio_mirna_clicked)
		self.ui.radio_isomir.toggled.connect(self.__radio_isomir_clicked)
		self.ui.combo_isoform.currentIndexChanged.connect(self.__combo_isoform_index_changed)
		self.ui.checkb_dump_to_csv.toggled.connect(self.__chk_dump_to_csv_clicked)
		self.ui.b_cancel.clicked.connect(self.__cancel_load_dataset_clicked)
		self.ui.b_load.clicked.connect(self.__load_dataset_clicked)

		# preprocessing tab
		self.ui.slider_split_dataset.valueChanged.connect(self.__slider_split_dataset_changed)
		self.ui.b_reset.clicked.connect(self.__b_reset_clicked)
		self.ui.b_apply.clicked.connect(self.__apply_preprocessing)

		# svm tab
		self.ui.chk_maj_voter.toggled.connect(self.__maj_voter_toggled)
		self.ui.dial_maj_voter.valueChanged.connect(self.__dial_maj_voter_changed)
		self.ui.b_reset_svm.clicked.connect(self.__reset_svm)
		self.ui.b_apply_svm.clicked.connect(self.__apply_svm)
		self.ui.b_roc_svm.clicked.connect(lambda: self.__show_roc('svm'))
		self.ui.b_report_svm.clicked.connect(lambda: self.__show_report('svm'))
		self.ui.b_dump_svm.clicked.connect(lambda: self.__dump_model('svm'))

		# random forest tab
		self.ui.b_reset_rf.clicked.connect(self.__reset_rf)
		self.ui.b_apply_rf.clicked.connect(self.__apply_rf)
		self.ui.b_roc_rf.clicked.connect(lambda: self.__show_roc('rf'))
		self.ui.b_report_rf.clicked.connect(lambda: self.__show_report('rf'))
		self.ui.b_dump_rf.clicked.connect(lambda: self.__dump_model('rf'))

		# neural network tab
		self.ui.radio_sgd.toggled.connect(self.__radio_sgd_toggled)
		self.ui.b_reset_nn.clicked.connect(self.__reset_nn)
		self.ui.b_apply_nn.clicked.connect(self.__apply_nn)
		self.ui.b_roc_nn.clicked.connect(lambda: self.__show_roc('nn'))
		self.ui.b_report_nn.clicked.connect(lambda: self.__show_report('nn'))
		self.ui.b_dump_nn.clicked.connect(lambda: self.__dump_model('nn'))
		

	####################################################################################################################
	################ signal handlers and functions, neural network tab #################################################
	@pyqtSlot()
	def __radio_sgd_toggled(self):
		if self.ui.radio_sgd.isChecked():
			self.ui.gb_learning_rate.setEnabled(True)
		else:
			self.ui.gb_learning_rate.setEnabled(False)

	@pyqtSlot()
	def __apply_nn(self):
		if self.ui.radio_lbfgd.isChecked():
			solver = 'lbfgs'
		elif self.ui.radio_sgd.isChecked():
			solver = 'sgd'
		elif self.ui.radio_adam.isChecked():
			solver = 'adam'
		else:
			QMessageBox.warning(self, "Warning", "Please choose a solver", "Ok")
			return

		if solver is 'sgd':
			if self.ui.radio_constant.isChecked():
				learning_rate = 'constant'
			elif self.ui.radio_invscaling.isChecked():
				learning_rate = 'invscaling'
			elif self.ui.radio_adaptive.isChecked():
				learning_rate = 'adaptive'
			else:
				QMessageBox.warning(self, "Warning", "Please choose a solver", "Ok")
				return
		else:
			learning_rate = 'constant'

		alpha = self.ui.spin_alpha.value()
		max_iterations = self.ui.spin_max_iter.value()

		try:
			hidden_layer_sizes = [int(s) for s in (str(self.ui.le_hideen_layer_sizes.text())).split(',') if len(s) > 0]
		except Exception as e:
			QMessageBox.critical(self, "Warning", "Please insert only positive integers separated by comma", "Ok")
			return
		for l in hidden_layer_sizes:
			if l <= 0:
				QMessageBox.critical(self, "Warning", "Please insert only positive integers separated by comma", "Ok")
				return

		print solver, learning_rate, alpha, tuple(hidden_layer_sizes), max_iterations
		# do it
		self.__setup_progress_trigger()
		self.progress_trigger.showProgress.emit("Training neural network")

		self.genericThread = GenericThread(self.__main.train_nn, solver, learning_rate, alpha, tuple(hidden_layer_sizes), max_iterations)
		self.connect(self.genericThread, QtCore.SIGNAL('finished()'), lambda: self.__training_callback('nn'))
		self.genericThread.start()
		

	@pyqtSlot()
	def __reset_nn(self):
		# disable results
		self.ui.gb_results_nn.setEnabled(False)
		self.ui.lab_accuracy_nn.setText('0')
		self.ui.lab_f1score_nn.setText('0')
		self.ui.lab_tpr_nn.setText('0')
		self.ui.lab_tnr_nn.setText('0')
		self.ui.lab_ppv_nn.setText('0')
		self.ui.lab_npv_nn.setText('0')
		self.ui.lab_fpr_nn.setText('0')
		self.ui.lab_fnr_nn.setText('0')
		# reset kernel
		self.ui.radio_lbfgd.setAutoExclusive(False)
		self.ui.radio_sgd.setAutoExclusive(False)
		self.ui.radio_adam.setAutoExclusive(False)
		self.ui.radio_lbfgd.setChecked(False)
		self.ui.radio_sgd.setChecked(False)
		self.ui.radio_adam.setChecked(False)
		self.ui.radio_lbfgd.setAutoExclusive(True)
		self.ui.radio_sgd.setAutoExclusive(True)
		self.ui.radio_adam.setAutoExclusive(True)
		# reset params
		self.ui.spin_alpha.setValue(0.0001)
		self.ui.le_hideen_layer_sizes.setText("100,")
		self.ui.spin_max_iter.setValue(200)


	####################################################################################################################
	################ signal handlers and functions, random forest tab ##################################################
	@pyqtSlot()
	def __apply_rf(self):
		if self.ui.radio_random.isChecked():
			type = 'randomized'
		elif self.ui.radio_extra.isChecked():
			type = 'extremely randomized'
		else:
			QMessageBox.critical(self, "Warning", "Please choose a forest type", "Ok")
			return
		
		if self.ui.radio_gini.isChecked():
			criterion = 'gini'
		elif self.ui.radio_entropy.isChecked():
			criterion = 'entropy'
		else:
			QMessageBox.critical(self, "Warning", "Please choose a split criterion", "Ok")
			return
		
		estimators = self.ui.spin_nestimators.value()
		max_depth = self.ui.spin_max_depth.value()
		bootstrap = self.ui.chk_bootstrap.isChecked()
		
		if self.ui.chk_maj_voter.isChecked():
			n_svm = int(self.ui.lab_num_svm.text())
		else:
			n_svm = 1

		print type, criterion, estimators, max_depth, bootstrap
		# do it
		self.__setup_progress_trigger()
		self.progress_trigger.showProgress.emit("Training random forest")

		self.genericThread = GenericThread(self.__main.train_rf, type, criterion, estimators, max_depth, bootstrap)
		self.connect(self.genericThread, QtCore.SIGNAL('finished()'), lambda: self.__training_callback('rf'))
		self.genericThread.start()

	@pyqtSlot()
	def __reset_rf(self):
		# disable results
		self.ui.gb_results_rf.setEnabled(False)
		self.ui.lab_accuracy_rf.setText('0')
		self.ui.lab_f1score_rf.setText('0')
		self.ui.lab_tpr_rf.setText('0')
		self.ui.lab_tnr_rf.setText('0')
		self.ui.lab_ppv_rf.setText('0')
		self.ui.lab_npv_rf.setText('0')
		self.ui.lab_fpr_rf.setText('0')
		self.ui.lab_fnr_rf.setText('0')
		# reset kernel
		self.ui.radio_random.setAutoExclusive(False)
		self.ui.radio_extra.setAutoExclusive(False)
		self.ui.radio_gini.setAutoExclusive(False)
		self.ui.radio_entropy.setAutoExclusive(False)
		self.ui.radio_random.setChecked(False)
		self.ui.radio_extra.setChecked(False)
		self.ui.radio_gini.setChecked(False)
		self.ui.radio_entropy.setChecked(False)
		self.ui.radio_random.setAutoExclusive(True)
		self.ui.radio_extra.setAutoExclusive(True)
		self.ui.radio_gini.setAutoExclusive(True)
		self.ui.radio_entropy.setAutoExclusive(True)
		# disable majority voter
		self.ui.chk_bootstrap.setChecked(True)
		# reset params
		self.ui.spin_nestimators.setValue(100)
		self.ui.spin_max_depth.setValue(3)


	####################################################################################################################
	################ signal handlers and functions, svm tab ############################################################
	@pyqtSlot(bool)
	def __maj_voter_toggled(self, toggled):
		if toggled:
			self.ui.dial_maj_voter.setEnabled(True)
		else:
			self.ui.dial_maj_voter.setEnabled(False)


	@pyqtSlot(int)
	def __dial_maj_voter_changed(self, value):
		self.ui.lab_num_svm.setText(unicode(2 * value + 1))


	@pyqtSlot()
	def __apply_svm(self):
		if self.ui.radio_linear.isChecked():
			kernel = 'linear'
		elif self.ui.radio_poly.isChecked():
			kernel = 'poly'
		elif self.ui.radio_rbf.isChecked():
			kernel = 'rbf'
		else:
			QMessageBox.critical(self, "Warning", "Please choose a kernel type", "Ok")
			return
		C = self.ui.spin_c.value()
		gamma = self.ui.spin_gamma.value()
		degree = self.ui.spin_degree.value()
		if self.ui.chk_maj_voter.isChecked():
			n_svm = int(self.ui.lab_num_svm.text())
		else:
			n_svm = 1

		print kernel, C, gamma, degree, n_svm
		# do it
		self.__setup_progress_trigger()
		self.progress_trigger.showProgress.emit("Training svm")

		self.genericThread = GenericThread(self.__main.train_svm, kernel, C, gamma, degree, n_svm)
		self.connect(self.genericThread, QtCore.SIGNAL('finished()'), lambda: self.__training_callback('svm'))
		self.genericThread.start()


	@pyqtSlot()
	def __reset_svm(self):
		# disable results
		self.ui.gb_results_svm.setEnabled(False)
		self.ui.lab_accuracy_svm.setText('0')
		self.ui.lab_f1score_svm.setText('0')
		self.ui.lab_tpr_svm.setText('0')
		self.ui.lab_tnr_svm.setText('0')
		self.ui.lab_ppv_svm.setText('0')
		self.ui.lab_npv_svm.setText('0')
		self.ui.lab_fpr_svm.setText('0')
		self.ui.lab_fnr_svm.setText('0')
		# reset kernel
		self.ui.radio_linear.setAutoExclusive(False)
		self.ui.radio_poly.setAutoExclusive(False)
		self.ui.radio_rbf.setAutoExclusive(False)
		self.ui.radio_linear.setChecked(False)
		self.ui.radio_poly.setChecked(False)
		self.ui.radio_rbf.setChecked(False)
		self.ui.radio_linear.setAutoExclusive(True)
		self.ui.radio_poly.setAutoExclusive(True)
		self.ui.radio_rbf.setAutoExclusive(True)
		# disable majority voter
		self.ui.chk_maj_voter.setChecked(False)
		# reset params
		self.ui.spin_c.setValue(1.0)
		self.ui.spin_gamma.setValue(0.0005)
		self.ui.spin_degree.setValue(3)


	####################################################################################################################
	################ common functions ##################################################################################
	def __training_callback(self, algorithm):
		# setattr(x, 'foobar', 123) is equivalent to x.foobar = 123 # ma non si può usare...
		result = self.__main.status
		self.progress_trigger.closeProgress.emit()
		if result is True:
			QMessageBox.information(self, "Success", "Learning done", "Ok")
			report = self.__main.reports[algorithm]
			if algorithm is 'svm':
				self.ui.gb_results_svm.setEnabled(True)
				self.ui.lab_accuracy_svm.setText(str(report['accuracy']))
				self.ui.lab_f1score_svm.setText(str(report['f1']))
				self.ui.lab_tpr_svm.setText(str(report['tpr']))
				self.ui.lab_tnr_svm.setText(str(report['tnr']))
				self.ui.lab_ppv_svm.setText(str(report['ppv']))
				self.ui.lab_npv_svm.setText(str(report['npv']))
				self.ui.lab_fpr_svm.setText(str(report['fpr']))
				self.ui.lab_fnr_svm.setText(str(report['fnr']))
			elif algorithm is 'rf':
				self.ui.gb_results_rf.setEnabled(True)
				self.ui.lab_accuracy_rf.setText(str(report['accuracy']))
				self.ui.lab_f1score_rf.setText(str(report['f1']))
				self.ui.lab_tpr_rf.setText(str(report['tpr']))
				self.ui.lab_tnr_rf.setText(str(report['tnr']))
				self.ui.lab_ppv_rf.setText(str(report['ppv']))
				self.ui.lab_npv_rf.setText(str(report['npv']))
				self.ui.lab_fpr_rf.setText(str(report['fpr']))
				self.ui.lab_fnr_rf.setText(str(report['fnr']))
			elif algorithm is 'nn':
				self.ui.gb_results_nn.setEnabled(True)
				self.ui.lab_accuracy_nn.setText(str(report['accuracy']))
				self.ui.lab_f1score_nn.setText(str(report['f1']))
				self.ui.lab_tpr_nn.setText(str(report['tpr']))
				self.ui.lab_tnr_nn.setText(str(report['tnr']))
				self.ui.lab_ppv_nn.setText(str(report['ppv']))
				self.ui.lab_npv_nn.setText(str(report['npv']))
				self.ui.lab_fpr_nn.setText(str(report['fpr']))
				self.ui.lab_fnr_nn.setText(str(report['fnr']))
		else:
			print "__training_callback (" + algorithm + "): err"
			print self.__main.err_msg
			QMessageBox.critical(self, "Error while training " + algorithm, self.__main.err_msg, "Ok")

	@pyqtSlot(str)
	def __show_report(self, algorithm):
		plot_classification_report((self.__main.reports[algorithm])['sklearn_report'])
		plt.show()

	@pyqtSlot(str)
	def __show_roc(self, algorithm):
		print "roc:", algorithm
		report = self.__main.reports[algorithm]
		plot_roc(report['roc'], algorithm)
		plt.show()


	####################################################################################################################
	################ signal handlers and functions, preprocessing tab ##################################################
	@pyqtSlot()
	def __apply_preprocessing(self):
		# 1. split dataset
		test_set_dim = (100.0 - int(self.ui.slider_split_dataset.value())) / 100.0
		if test_set_dim == 1.0:
			QMessageBox.critical(self, "Warning", "The training set cannot be empty", "Ok")
			return
		elif test_set_dim == 0.0:
			QMessageBox.critical(self, "Warning", "The test set cannot be empty", "Ok")
			return
		elif test_set_dim > 50.0:
			QMessageBox.warning(self, "Warning",
			                    "Be careful the training set is large enough for learning to be effective",
			                    "Ok")

		# 2. scale features
		if self.ui.radio_std_scaler.isChecked():
			scaler = "standard"
		elif self.ui.radio_minmax_scaler.isChecked():
			scaler = "minmax"
		elif self.ui.radio_robust_scaler.isChecked():
			scaler = "robust"
		else:
			QMessageBox.critical(self, "Warning", "Please choose a scaler", "Ok")
			return

		if not self.ui.chk_var_threshold.isChecked() and not self.ui.chk_kbest.isChecked() and not self.ui.chk_pca.isChecked():
			QMessageBox.warning(self, "Warning",
			                    "Feature selection is an important step to avoid overfitting",
			                    "Ok")

		# 3. feature selection
		fs = {}
		if self.ui.chk_var_threshold.isChecked():
			var_threshold = round(float(self.ui.spin_variance_threshold.value()), 5)
			if var_threshold == 0.0:
				QMessageBox.critical(self, "Warning", "Variance threshold cannot be zero", "Ok")
				return
			fs["variance_threshold"] = var_threshold
		if self.ui.chk_kbest.isChecked():
			kbest_features = int(self.ui.spin_kbest_features.value())
			if kbest_features == 0 or kbest_features >= self.__main.n_features:
				QMessageBox.critical(self, "Warning",
				                     "k must be greater than zero and less than " + str(self.__main.n_features), "Ok")
				return
			fs["kbest"] = kbest_features
		if self.ui.chk_pca.isChecked():
			fs["pca"] = True

		print test_set_dim, scaler, fs

		# do it
		self.__setup_progress_trigger()
		self.progress_trigger.showProgress.emit("Preprocessing data")

		self.genericThread = GenericThread(self.__main.preprocessing, test_set_dim, scaler, fs)
		self.connect(self.genericThread, QtCore.SIGNAL('finished()'), self.__data_preprocessing_callback)
		self.genericThread.start()

	def __data_preprocessing_callback(self):
		result = self.__main.status
		self.progress_trigger.closeProgress.emit()
		if result is True:
			print "__data_preprocessing_callback: ok"
			self.ui.tab_svm.setEnabled(True)
			self.ui.tab_rf.setEnabled(True)
			self.ui.tab_nn.setEnabled(True)
			self.__fill_preprocessing_stats()
			QMessageBox.information(self, "Success", "Preprocessing done", "Ok")

		else:
			print "__data_preprocessing_callback: err"
			print self.__main.err_msg
			QMessageBox.critical(self, "Error while preprocessing data", self.__main.err_msg, "Ok")
			self.ui.tab_svm.setEnabled(False)
			self.ui.tab_rf.setEnabled(False)
			self.ui.tab_nn.setEnabled(False)

	def __fill_preprocessing_stats(self):
		self.ui.lab_train_test_split.setText(unicode("#training samples / #test samples\t" + str(self.__main.X_train.shape[0]) + " / " + str(self.__main.X_test.shape[0])))
		self.ui.lab_features_before_selection.setText(unicode("#features before preprocessing\t" + str(self.__main.X_train.shape[1])))
		self.ui.lab_features_after_selection.setText(unicode("#features after preprocessing\t" + str(self.__main.X_train_sel.shape[1])))


	@pyqtSlot()
	def __b_reset_clicked(self):
		# reset fields
		self.ui.slider_split_dataset.setValue(80)
		self.ui.spin_variance_threshold.setValue(0.0014)
		self.ui.spin_kbest_features.setValue(2500)
		self.ui.chk_var_threshold.setChecked(False)
		self.ui.chk_kbest.setChecked(False)
		self.ui.chk_pca.setChecked(False)
		# uncheck radio buttons
		self.ui.radio_std_scaler.setAutoExclusive(False)
		self.ui.radio_minmax_scaler.setAutoExclusive(False)
		self.ui.radio_robust_scaler.setAutoExclusive(False)
		self.ui.radio_std_scaler.setChecked(False)
		self.ui.radio_minmax_scaler.setChecked(False)
		self.ui.radio_robust_scaler.setChecked(False)
		self.ui.radio_std_scaler.setAutoExclusive(True)
		self.ui.radio_minmax_scaler.setAutoExclusive(True)
		self.ui.radio_robust_scaler.setAutoExclusive(True)
		# disable learning tabs
		self.ui.tab_svm.setEnabled(False)
		self.ui.tab_rf.setEnabled(False)
		self.ui.tab_nn.setEnabled(False)

	@pyqtSlot(int)
	def __slider_split_dataset_changed(self, value):
		self.ui.perc_train_set.setText(unicode(value))
		self.ui.perc_test_set.setText(unicode(100 - value))


	####################################################################################################################
	################ signal handlers and functions, data tab ###########################################################
	@pyqtSlot(bool)
	def __radio_from_csv_clicked(self, enabled):
		self.csv_source = None
		if enabled:
			self.ui.gb_from_csv.setEnabled(True)
			self.ui.gb_features.setEnabled(True)
		else:
			self.ui.gb_from_csv.setEnabled(False)
			self.ui.gb_features.setEnabled(False)

	@pyqtSlot(bool)
	def __pick_csv(self):
		sel = unicode(QFileDialog.getOpenFileName(self, 'Open File', './data', 'CSV files (*.csv)',
		                                          options=QFileDialog.DontUseNativeDialog))
		if len(sel) > 0:
			print sel
			self.csv_source = sel
			self.ui.ledit_csv_name.setText(self.csv_source)

	@pyqtSlot(bool)
	def __radio_from_database_clicked(self, enabled):
		if enabled:
			self.ui.gb_from_db.setEnabled(True)
			self.ui.gb_features.setEnabled(True)
			#self.ui.combo_isoform.setEnabled(False)
			#self.ui.combo_isoform.setCurrentIndex(-1)
		else:
			self.ui.gb_from_db.setEnabled(False)
			self.ui.gb_features.setEnabled(False)

	@pyqtSlot(bool)
	def __radio_mirna_clicked(self, enabled):
		if enabled:
			pass

	@pyqtSlot(bool)
	def __radio_isomir_clicked(self, enabled):
		if enabled:
			self.ui.combo_isoform.setEnabled(True)
		else:
			self.ui.combo_isoform.setEnabled(False)
		# self.ui.combo_isoform.setCurrentIndex(-1)

	@pyqtSlot()
	def __combo_isoform_index_changed(self, *args, **kwargs):
		pass
		# text = unicode(self.ui.combo_isoform.currentText())
		# if len(text) > 0:
		#	self.isoform = text
		# else:
		#	self.isoform = None


	@pyqtSlot(bool)
	def __chk_dump_to_csv_clicked(self, enabled):
		if enabled:
			filename = unicode(QtGui.QFileDialog.getSaveFileName(self, 'Save File', '.', 'CSV files (*.csv)',
			                                                     options=QFileDialog.DontUseNativeDialog))
			if len(filename) > 0:
				self.ui.lab_csv_out.setText(unicode(filename))
		else:
			pass
		# self.ui.lab_csv_out.setText(unicode(""))

	@pyqtSlot()
	def __cancel_load_dataset_clicked(self, *args, **kwargs):
		self.ui.gb_from_db.setEnabled(False)
		self.csv_source = None
		self.isoform = None
		self.ui.radio_from_csv.setAutoExclusive(False)
		self.ui.from_database_radio.setAutoExclusive(False)
		self.ui.radio_mirna.setAutoExclusive(False)
		self.ui.radio_isomir.setAutoExclusive(False)

		self.ui.radio_from_csv.setChecked(False)
		self.ui.from_database_radio.setChecked(False)
		self.ui.radio_isomir.setChecked(False)
		self.ui.radio_mirna.setChecked(False)

		self.ui.radio_from_csv.setAutoExclusive(True)
		self.ui.radio_from_csv.setAutoExclusive(True)
		self.ui.from_database_radio.setAutoExclusive(True)
		self.ui.radio_mirna.setAutoExclusive(True)
		self.ui.radio_isomir.setAutoExclusive(True)

		self.__setup()

	@pyqtSlot()
	def __load_dataset_clicked(self, *args, **kwargs):
		#disable all tabs except the data one
		self.ui.tab_preprocessing.setEnabled(False)
		self.ui.tab_svm.setEnabled(False)
		self.ui.tab_rf.setEnabled(False)
		self.ui.tab_nn.setEnabled(False)

		csv_name = None
		target_label = None

		if self.ui.radio_from_csv.isChecked():
			csv_name = str(self.ui.ledit_csv_name.text())
			if csv_name is None or len(csv_name) <= 0:
				QMessageBox.warning(self, "!", "Missing csv name", "Ok")
				return

		elif self.ui.from_database_radio.isChecked():
			target_label = unicode(self.ui.ledit_target_label.text())
			if target_label is None or len(target_label) <= 0:
				QMessageBox.warning(self, "!", "Missing target label", "Ok")
				return
		else:
			QMessageBox.warning(self, "!", "Select a data source", "Ok")
			return


		if self.ui.radio_isomir.isChecked():
			print "qua"
			use_isoforms = True
			iso = unicode(self.ui.combo_isoform.currentText())

		elif self.ui.radio_mirna.isChecked():
			print "là"
			use_isoforms = False
			iso = None
		else:
			QMessageBox.warning(self, "!", "Select a feature type", "Ok")
			return

		self.__setup_progress_trigger()
		self.progress_trigger.showProgress.emit("Loading data")

		self.genericThread = GenericThread(self.__main.get_data, csv_name, target_label, use_isoforms, iso)
		self.connect(self.genericThread, QtCore.SIGNAL('finished()'), self.__data_loaded_callback)
		self.genericThread.start()




	def __data_loaded_callback(self):
		self.ui.list_classes.clear()
		result = self.__main.status
		self.progress_trigger.closeProgress.emit()
		if result is True:
			print "__data_loaded_callback: ok"
			self.ui.tab_preprocessing.setEnabled(True)
			self.__fill_dataset_stats()
			QMessageBox.information(self, "Success", "Dataset loaded", "Ok")

		else:
			print "__data_loaded_callback: err"
			print self.__main.err_msg
			QMessageBox.critical(self, "Error while loading data", self.__main.err_msg, "Ok")
			self.ui.tab_preprocessing.setEnabled(False)

	def __fill_dataset_stats(self):
		self.ui.gb_stats.setVisible(True)
		print self.__main.n_samples
		self.ui.lab_n_samples.setText(str(self.__main.n_samples) + " samples")
		self.ui.lab_n_features.setText(str(self.__main.n_features) + " features")
		self.ui.lab_n_mirna.setText(str(self.__main.n_mirna) + " miRNA")
		self.ui.lab_n_isomir.setText(str(self.__main.n_isomir) + " isomiR")
		for k in self.__main.classes_distr:
			self.ui.list_classes.addItem(k + "\t" + str(self.__main.classes_distr[k]) + " observations")


	####################################################################################################################
	################ progress bar ######################################################################################
	def __close_progress(self):
		self.ui.tabWidget.setEnabled(True)  # enable whole interface
		self.progress_bar.close()

	def __show_progress(self, msg):
		self.ui.tabWidget.setEnabled(False)  # disable whole interface
		print "show progress"
		self.progress_bar = QProgressDialog(self)
		self.progress_bar.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
		self.progress_bar.setMinimumSize(400, 150)
		self.progress_bar.setMaximumSize(400, 150)
		self.progress_bar.setWindowTitle("Please wait...")
		self.progress_bar.setLabelText(msg)
		self.progress_bar.setMinimum(0)
		self.progress_bar.setMaximum(0)
		self.progress_bar.canceled.connect(self.__on_progress_cancel)
		self.progress_bar.show()

	def __on_progress_cancel(self):
		print "on progress cancel"
		if self.genericThread is not None:
			self.genericThread.terminate()
			QThread.wait(QThread(self.genericThread), 0)
			self.genericThread = None
		self.progress_trigger = None
		self.__close_progress()


	def __setup_progress_trigger(self):
		self.progress_trigger = Progress()
		self.progress_trigger.showProgress.connect(self.__show_progress, QtCore.Qt.DirectConnection)
		self.progress_trigger.closeProgress.connect(self.__close_progress, QtCore.Qt.DirectConnection)


########################################################################################################################
################ main ##################################################################################################
def main():
	app = QtGui.QApplication(sys.argv)
	myapp = UiController()
	myapp.show()
	sys.exit(app.exec_())


if __name__ == '__main__':
	main()
