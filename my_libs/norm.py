#!/usr/bin/python
# -*- coding: utf-8 -*-

import constants as const
import utils
import sys, os
from sys import getsizeof
import logging
from threading import Thread
import time
import Queue
from pymongo import MongoClient
import random
from retrying import retry
from bson.code import Code
import time
import pandas as pd
import argparse

def load_dataset_from_label(label, isoforms=False, dump_to=None):
	try:
		host, port, db_name = utils.get_db_params()
	except Exception as e:
		print e		
	try:
		client = MongoClient(host, port)
		db = client[db_name]
	except Exception as e:
		raise Exception(e)

	#samples = list(db.sample_collection.find({"labels."+label: {"$exists": True}}, { "labels.$": 1, "name": 1, "mapped_reads": 1, "tags": 1, "_id": 0}))
	#do not return list of tag ids
	#samples = list(db.sample_collection.find({"labels."+label: {"$exists": True}}, { "labels."+label: 1, "name": 1, "mapped_reads": 1, "_id": 0}))
	samples = list(db.sample_collection.aggregate(
		[
			{"$match": {"labels."+label: {"$exists": True}}},
			{"$project": { "label": "$labels."+label, "name": 1, "mapped_reads": 1, "_id": 0}}
		]
	))
	samples = dict((item['name'], item) for item in samples)
	if len(samples) == 0:
		raise Exception("In load_dataset_from_label: 0 samples found with label " + label)
	
	tag_ids = list()
	#tag ids not returned in previous query
	#for s in samples.values():
		#tag_ids.extend(s["tags"])
		#print "{} {}".format(s["name"], len(s["tags"]))	#mettere nel log
	#print "{} samples retrieved; {} tags".format(len(samples), len(tag_ids)) #idem
	print "{} samples retrieved".format(len(samples))
	
	# $match... $in (see below) is a little slower than iterating through samples
	t_mir = list()
	t_isomir = list()	
	tic = time.clock()
	for s in samples:	
		t_mir.extend(list(db.tag_collection.aggregate(
			[
				{"$match": {"sample_name":s}},
				{"$group": {"_id": {"sample": "$sample_name", "mirna": "$mirna_name"}, "count": {"$sum": "$count"}}},
				{"$project": {"_id" : 0, "sample": "$_id.sample", "isomir": "$_id.mirna", "count": "$count"}}
				#{"$out": "prova1"}
			],
			allowDiskUse = True		
		)))
		if isoforms is True:
			t_isomir.extend(list(db.tag_collection.aggregate(
				[
					{"$match": {"sample_name":s}},
					{"$group": {"_id":{"sample": "$sample_name", "mirna":"$mirna_name", "iso": "$isoform"}, "count":{"$sum":"$count"}}},
					{"$project": {"_id" : 0, "sample": "$_id.sample", "isomir": {"$concat": ["$_id.mirna", " ", "$_id.iso"]}, "count": "$count"}}
					#{"$out": "prova2"}
				],
				allowDiskUse = True
			)))
	toc = time.clock()
	print "Elapsed time: {} s".format(toc - tic)

	
	print len(t_mir), len(t_isomir)
	dft = pivot_and_normalize(samples, t_mir, t_isomir)
	data = dft.ix[:, dft.columns != 'label'] # ix è un indexer che accetta numeri ed etichette; aggiungere 'label' a get_tags_from_labels e sostituire al mirna di prova!
	target = dft.ix[:, ['label']]	# estrae la colonna delle etichette
	if dump_to is not None:
		try:
			dft.to_csv(dump_to, encoding='utf-8')
		except Exception as e:
			print "In norm.load_dataset_from_label: data could not be dumped to file", dump_to
			print e
	return data, target


def pivot_and_normalize(samples, t_mir, t_isomir):
	sample_features = {}
	mir_count_by_sample = {}
	for item in t_mir:
		sample_name = item["sample"]		
		if sample_name not in sample_features:
			sample_features[sample_name] = {}
		if sample_name not in mir_count_by_sample:
			mir_count_by_sample[sample_name] = {}
		c = float(item["count"])
		mir_count_by_sample[sample_name][item["isomir"]] = c
		if len(t_isomir) == 0:
			sample_features[sample_name][item["isomir"]] = c / samples[sample_name]["mapped_reads"]
		

	if len(t_isomir) != 0:
		for item in t_isomir:
			sample_name = item["sample"]
			if sample_name not in sample_features:
				sample_features[sample_name] = {}
				#print item["isomir"].split()[1]
			sample_features[sample_name][item["isomir"]] = 100.0 * item["count"] / mir_count_by_sample[sample_name][item["isomir"].split()[0]] 
	
	for s in samples.values():
		sample_features[s["name"]]["label"] = s["label"]

	data = pd.DataFrame(sample_features).fillna(0)
	return data.T # trasposta: sample sulle righe, feature sulle colonne

	
def norm(it, base, how="mapped"):
	pass


def load_dataset_from_file(from_csv):
	# import data from csv file
	try:
		dft = pd.read_csv(from_csv, encoding='utf-8')
		dft = dft.drop(dft.columns[[0]], axis=1)
		data = dft.ix[:, dft.columns != 'label'] # ix è un indexer che accetta numeri ed etichette; aggiungere 'label' a get_tags_from_labels e sostituire al mirna di prova!
		target = dft.ix[:, ['label']]	# estrae la colonna delle etichette
		return data, target
	except Exception as e:
		raise e
