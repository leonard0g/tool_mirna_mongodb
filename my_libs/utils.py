#!/usr/bin/python
# -*- coding: utf-8 -*-

# mappe e indici di isoforme e interaction site dovrebbero essere qua: trasformare questo file in una classe?

from ConfigParser import SafeConfigParser, ParsingError
import sys, os
import logging


def prompt(message, error_message, isvalid):
	"""Prompt for input given a message and return that value after verifying the input.
	Keyword arguments:
	message -- the message to display when asking the user for the value
	error_message -- the message to display when the value fails validation
	isvalid -- a function that returns True if the value given by the user is valid
	"""
	res = None
	while res is None:
		res = raw_input(str(message) + ': ')
		if isvalid is not None:
			if not isvalid(res):
				print str(error_message)
				res = None
	return res


def get_labels():
	labels = prompt(
		message="Enter two or more labels separated by space",
		error_message="Labels not valid",
		isvalid=lambda v: len(set(v.split())) >= 2)
	return list(set(labels.split()))


def get_label():
	label = prompt(
		message="Enter a label",
		error_message="Label not valid",
		isvalid=lambda v: len(v.strip()) >= 1)
	return label


def build_isoforms_and_interaction_sites():
	parser = SafeConfigParser()
	parser.read(".config.rc")

	isoform_index = list()
	section = "isoform index"
	for i in parser.options(section):
		isoform_index.append(int(parser.get(section, i)))
	isoforms = {}
	for isoform in parser.options("isoforms"):
		isoforms[int(parser.get("isoforms", isoform))] = isoform

	interaction_site_index = list()
	section = "interaction site index"
	for i in parser.options(section):
		interaction_site_index.append(int(parser.get(section, i)))
	interaction_sites = {}
	for int_site in parser.options("interaction sites"):
		interaction_sites[int(parser.get("interaction sites", int_site))] = int_site

	return isoform_index, isoforms, interaction_site_index, interaction_sites


def get_tag_filters():
	filters = {"idDataset": None, "idSample": None, "idMirna": None, "idIsoform": None, "idInteractionSite": None,
	           "labels": None}
	for k, v in filters.items():
		val = prompt(
			message="Enter %s value (default: %s)" % (k, v),
			errormessage="",
			isvalid=None)
		if not val == "":
			filters[k] = val
	return filters


def get_file_path():
	file_path = prompt(
		message="File path",
		error_message="The file does not exist",
		isvalid=lambda v: os.path.isfile(os.path.expanduser(v)))
	if file_path is None:
		raise Exception("In utils.get_file.path: path is None")
	while len(file_path) >= 2 and (file_path[-1] == '/' or file_path[-1] == '\\'):
		file_path = file_path[:-1]
	return os.path.expanduser(file_path)


def get_folder_path():
	folder_path = prompt(
		message="Folder path",
		error_message="The folder does not exist",
		isvalid=lambda v: os.path.isdir(os.path.expanduser(v)))
	if folder_path is None:
		raise Exception("In utils.get_folder_path: path is None")
	while len(folder_path) >= 2 and (folder_path[-1] == '/' or folder_path[-1] == '\\'):
		folder_path = folder_path[:-1]
	return os.path.expanduser(folder_path)


def get_db_params():
	params = {}
	parser = SafeConfigParser()
	parser.read(".config.rc")

	section = "db connection params"
	# options = parser.options(section)
	options = ["host", "port", "db"]
	for option in options:
		try:
			params[option] = parser.get(section, option)
			if params[option] == -1:
				Logging.warning("Skipping %s! Check configuration file", option)
		except Exception as e:
			raise e
	return params["host"], int(params["port"]), params["db"]


def dict_list_to_dict(dict_list, k, v):
	d = dict()
	for item in dict_list:
		d[item[k]] = item[v]
	return d
